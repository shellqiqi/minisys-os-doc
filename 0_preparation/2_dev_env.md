# 开发环境

- [开发环境](#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)
  - [硬件](#%E7%A1%AC%E4%BB%B6)
  - [软件](#%E8%BD%AF%E4%BB%B6)

## 硬件

+ Minisys-1开发板
+ BusBlaster下载调试板
+ MIPSfpga SOC 1.0 (MIPSfpga2.0)
+ PC机

## 软件

+ Windows 10 操作系统
+ Vivado 2017.4 工具套件
+ OpenOCD-img-0.10.0.3 (包含GCC编译器)
+ Codescape MIPS SDK Essentials(带 MIPSfpga Getting Started Package) 
+ VScode
+ 其他开发Windows应用程序的软件

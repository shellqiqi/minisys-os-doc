# 硬件

- [硬件](#%E7%A1%AC%E4%BB%B6)
  - [前期要求](#%E5%89%8D%E6%9C%9F%E8%A6%81%E6%B1%82)
  - [针对minisys os的修改](#%E9%92%88%E5%AF%B9minisys-os%E7%9A%84%E4%BF%AE%E6%94%B9)

## 前期要求

首先保证平台是根据 https://gitlab.com/shellqiqi/mips-fpga-soc-on-minisys 来搭建并且能够运行文档内构建的Linux.

## 针对minisys os的修改

我们在这个平台的基础上需要删减和添加一些外设, 最后保留的外设有:

![](img/1_1.png)

两个需要注意的地方:

1. 唯一添加的外设是axi timer, 请参考文档进行添加.
2. 删除了axi intc, axi timer的中断直接接在0号硬件中断上, 串口中断不使用.

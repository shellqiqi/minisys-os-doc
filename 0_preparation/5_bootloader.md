# bootloader

- [bootloader](#bootloader)
  - [初始化硬件](#%E5%88%9D%E5%A7%8B%E5%8C%96%E7%A1%AC%E4%BB%B6)
  - [组装异常入口](#%E7%BB%84%E8%A3%85%E5%BC%82%E5%B8%B8%E5%85%A5%E5%8F%A3)
  - [初始化软件](#%E5%88%9D%E5%A7%8B%E5%8C%96%E8%BD%AF%E4%BB%B6)

## 初始化硬件

现在终于轮到我们的设备可以启动了. 根据上一节lds的描述, 我们的启动代码`boot/boot.S`来负责初始化硬件的事情.

启动的第一件事情就是清除cp0 count, 一个用来测量启动时间的寄存器. 然后初始化status寄存器, cause寄存器和compare寄存器.

接下来对每一个tlb表项做清理.

然后清理icache和dcache, 为进入kseg0做准备.

做完之后经过跳转到`_start`, 我们终于可以顺利地运行到`boot/start.S`所写的代码了. 到目前为止还是汇编, 马上我们就能进入到C语言的世界了.

## 组装异常入口

在跟着`_start`继续之前先看一下except_vec3所描述的一场入口, 具体的介绍留在异常的那一节开始, 这里简要说一些结构.

这里有四个异常入口, 当遇到异常且开中断的时候, 会无条件跳转到这四个入口. 我们的代码将所有入口都再一次跳转到`_mips_general_exception`也就是`0x80000180`的位置去.

```s
nop
mfc0    k1,CP0_CAUSE
la      k0,exception_handlers
andi    k1,0x7c
addu    k0,k1
lw      k0,(k0)
nop
jr      k0
nop
```

这段代码的大致意思是用过cause寄存器的值来跳转到`exception_handlers`一个函数数组所指向的函数去, 这个函数就用来处理cause寄存器所描述的异常.

## 初始化软件

略过`boot/start.S`中间的一部分全局变量, 在运行我们的main函数之前我们要做一些额外的事情.

首先禁止中断, 这样才能保证软件初始化不被打断. 由于我们有EJTAG, watch exception也是不需要的, 同样清除掉.

接下来比较重要, 我们要清除bss段, 来保证我们未初始化的全局变量保持为0.

然后sp设置在`0x80400000`, 处于栈顶的位置.

最后清理boot interrupt vector bit, 通过`jal main`去往我们的main函数, 也就是C语言的世界.

# 内存管理

- [内存管理](#%E5%86%85%E5%AD%98%E7%AE%A1%E7%90%86)
  - [几个复杂的内存管理函数调用依赖图](#%E5%87%A0%E4%B8%AA%E5%A4%8D%E6%9D%82%E7%9A%84%E5%86%85%E5%AD%98%E7%AE%A1%E7%90%86%E5%87%BD%E6%95%B0%E8%B0%83%E7%94%A8%E4%BE%9D%E8%B5%96%E5%9B%BE)
    - [`misp_vm_init`](#mispvminit)
    - [`page_insert`和`page_lookup`](#pageinsert%E5%92%8Cpagelookup)

## 几个复杂的内存管理函数调用依赖图

原文档自下而上介绍这些函数，缺少全局感，难以理解，这里改为自上而下介绍。

### `misp_vm_init`

```
mips_vm_init
    boot_map_segment
        boot_pgdir_walk
            alloc
```

首先是`mips_vm_init`函数，该函数被拆解成三个函数来调用。`mips_vm_init`用来给操作系统分配页表、内存控制块数组和进程控制块数组分配物理内存。这里有三个变量是比较重要的，而且在该函数内初始化了，分别是页表`pgdir`, 内存控制块数组`pages`, 进程控制块数组`envs`。其中全局变量`mCONTEXT`被赋值为`pgdir`。

为了给这些变量分配内存，我们需要函数`alloc`来做这个事情。`alloc`分配指定字节的物理内存，然后返回新分配内存的首地址。所以以上三个变量的地址是`alloc`来分配的。

分配完了之后需要在页表内记录自己使用了这些页，这个工作由`boot_map_segment`来负责，它将虚拟地址与物理地址建立映射。注意到在`mips_vm_init`中对`boot_map_segment`的调用。

```c
boot_map_segment(pgdir, UPAGES, n, PADDR(pages), PTE_R);
boot_map_segment(pgdir, UENVS, n, PADDR(envs), PTE_R);
```

这里被映射的虚拟地址`UPAGES`和`UENVS`请参考上一篇内存布局来理解。

由于`boot_map_segment`接受不同大小的虚拟内存空间，当我们要记录进页表的时候需要把大于一页空间的内存拆成多个，那么轮到`boot_pgdir_walk`工作了。如果你先阅读了原文档你会知道我们的页表是一个二级页表的结构。`boot_pgdir_walk`以页为单位来工作，所以它会直接修改页表。

### `page_insert`和`page_lookup`

```
page_insert/page_lookup
    pgdir_walk
        page_alloc
```

`page_insert`插入一个页到虚拟地址开始的一页空间当中。首先要查一下该虚拟地址原来有些什么，这个工作由`pgdir_walk`来处理，然后会返回一个页表项，我们就可以根据这个页表项来插入新的页。插入的时候重新调用`pgdir_walk`，但是这次置`create`为真，使得我们的新页是空白的，并且返回的页表项可以用来判断我们的修改是否成功。

`page_lookup`查找虚拟地址所在的页，这个功能和`pgdir_walk`十分类似，但是我们不返回页表项而是要返回页，那么我们就应该对页表项的一些位做判断看是否要返回一个页，还是空指针。

`page_walk`如刚刚提到的，在`create`为0的时候返回相应的页表项，这里直接查表就可以了。而当`create`为1的时候，我们需要分配页，这个工作由`page_alloc`来做。然后我们修改页表来记录这一页的使用。

`page_alloc`需要取空页链表的第一个页，然后对该页初始化即可。
